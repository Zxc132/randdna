#include <string>
#include <random>


using namespace std;
using std::string;

string randDNA(int seed, string bases, int n)
{
	string seq; //sequence
	int min =0;
	int max =bases.size() -1;
	
	
	mt19937 eng(seed);
	uniform_int_distribution<>uni(min,max);
	
	for (int i=0; i<n;i++){
		seq += bases[uni(eng)];
	}	
	return seq;
}
